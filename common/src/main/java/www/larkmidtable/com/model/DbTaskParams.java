package www.larkmidtable.com.model;

import lombok.Getter;
import lombok.Setter;
import www.larkmidtable.com.bean.ConfigBean;

/**
 * 参数对象父类
 * @Description 每种db作业类型都有 conn和 split。
 * @Author Gooch
 **/
@Getter
@Setter
public class DbTaskParams extends TaskParams {
    protected String splitSql;

    public DbTaskParams(ConfigBean configBean) {
        this.configBean = configBean;
    }

    public DbTaskParams(ConfigBean configBean, String splitSql) {
        this.configBean = configBean;
        this.splitSql = splitSql;
    }
}
