package www.larkmidtable.com.util;

import www.larkmidtable.com.model.DbTaskResult;

import java.util.function.BinaryOperator;

public class FunctionUtil {

    public static BinaryOperator<DbTaskResult> reduceResultFunc() {
        return (t, t2) -> {
            if (t2 != null) {
                t2.setSuccess(t2.isSuccess() && t.isSuccess());
                t2.setSuccessRowNum(t2.getSuccessRowNum() + t.getSuccessRowNum());
                t2.setFailRowNum(t2.getFailRowNum() + t.getFailRowNum());
                t2.setDuration(Math.max(t2.getDuration(), t.getDuration()));
                return t2;
            } else {
                return t;
            }
        };
    }
}
