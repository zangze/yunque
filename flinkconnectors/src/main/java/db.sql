-- `alarm-sc`.vehicle_alarm_202104 definition
CREATE DATABASE `alarm-sc` /*!40100 DEFAULT CHARACTER SET utf8 */;

CREATE TABLE `vehicle_alarm_202104` (
  `id` varchar(100) DEFAULT NULL,
  `license_plate` varchar(100) DEFAULT NULL,
  `plate_color` varchar(100) DEFAULT NULL,
  `device_time` varchar(100) DEFAULT NULL,
  `zone` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
