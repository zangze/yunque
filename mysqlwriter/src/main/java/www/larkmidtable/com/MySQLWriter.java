package www.larkmidtable.com;

import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import www.larkmidtable.com.bean.ConfigBean;
import www.larkmidtable.com.model.DbTaskParams;
import www.larkmidtable.com.model.DbTaskResult;
import www.larkmidtable.com.writer.AbstractDBWriter;

/**
 *
 *
 * @Date: 2022/11/14 11:01
 * @Description:
 **/
public class MySQLWriter extends AbstractDBWriter {
	private static Logger logger = LoggerFactory.getLogger(MySQLWriter.class);

	/**
	 * 以下是读写解耦新版代码
	 */
	@Override
	public List<AbstractDbWritTask> getDBWriteTasks() {
		ArrayList<AbstractDbWritTask> mySqlWriteTasks = new ArrayList<>();
		ConfigBean configBean = getConfigBean();
		for (int i = 0; i < configBean.getThread(); i++) {
			mySqlWriteTasks.add(new MySQLWriteTask(new DbTaskParams(configBean)));
		}
		return mySqlWriteTasks;
	}

	public class MySQLWriteTask extends AbstractDbWritTask {

		public MySQLWriteTask(DbTaskParams taskParams) {
			super(taskParams);
		}

		@Override
		public void preProcess() {
			ConfigBean configBean = this.taskParams.getConfigBean();
			Properties properties = new Properties();
			properties.setProperty("user", configBean.getUsername());
			properties.setProperty("password", configBean.getPassword());
			properties.setProperty("rewriteBatchedStatements", "true");
			properties.setProperty("useServerPrepStmts", "true");
			try {
				this.connection = DriverManager.getConnection(configBean.getUrl(), properties);
				this.connection.setAutoCommit(false);
			} catch (SQLException e) {
				throw new RuntimeException(e);
			}
			dbTaskResult = new DbTaskResult();
		}
	}
}
