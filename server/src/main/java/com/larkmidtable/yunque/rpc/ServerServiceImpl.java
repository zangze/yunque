package com.larkmidtable.yunque.rpc;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.larkmidtable.yunque.config.ConfigConstant;
import com.larkmidtable.yunque.system.MachineInfo;
import org.apache.commons.cli.BasicParser;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.yaml.snakeyaml.Yaml;
import www.larkmidtable.com.bean.ConfigBean;
import www.larkmidtable.com.channel.ChannelV2;
import www.larkmidtable.com.channel.DefaultChannelV2;
import www.larkmidtable.com.exception.YunQueException;
import www.larkmidtable.com.log.LogRecord;
import www.larkmidtable.com.reader.Reader;
import www.larkmidtable.com.transformer.TransformerExecution;
import www.larkmidtable.com.transformer.TransformerInfo;
import www.larkmidtable.com.util.DBUtil;
import www.larkmidtable.com.util.ExitCode;
import www.larkmidtable.com.util.TransformerUtil;
import www.larkmidtable.com.writer.Writer;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 *
 * @Date: 2023/8/9 23:39
 * @Description:
 **/
public class ServerServiceImpl implements ServerService {

	private static final Logger logger = LoggerFactory.getLogger(ServerServiceImpl.class);

	public String executeSQL(Object args,int pos,int count) throws ParseException {
		String[] argA = ((String[]) args);
		logger.info("参数列表: " + Arrays.toString(argA));
		logger.info("解析传递的参数....");
		BasicParser parser = new BasicParser();

		Options options = new Options();
		options.addOption("job", true, "作业配置");
		options.addOption("jobId", true, "作业id");
		options.addOption("path", true, "作业文件路径");
		options.addOption("fileFormat", true, "作业文件格式 JSON或者YAML");
		options.addOption("server", true, "服务器的地址");

		CommandLine cl = parser.parse(options, argA);
		String jobName = cl.getOptionValue("job");
		String jobIdString = cl.getOptionValue("jobId");
		String path = cl.getOptionValue("path");
		String fileFormat = cl.getOptionValue("fileFormat");
		long jobId = -1;
		if (jobIdString != null && !"-1".equalsIgnoreCase(jobIdString)) {
			jobId = Long.parseLong(jobIdString);
		}
		logger.info("作业名称 {} ,作业ID {} ,作业的路径 {} ,作业文件的格式 {}", jobName , jobId , path ,fileFormat);

		logger.info("读取作业配置文件....");
		BufferedReader br;
		StringBuffer jsonBuffer =new StringBuffer();
		try {
			br = new BufferedReader(new FileReader(path));
		} catch (FileNotFoundException e) {
			throw new YunQueException("作业文件路径获取不到，核查参数path的配置....", e);
		}
		Map<String, Map<String, String>> jobMap = new HashMap<>();
		if("JSON".equals(fileFormat)) {
			try {
				String contentLine = br.readLine();
				while (contentLine != null) {
					jsonBuffer.append(contentLine);
					contentLine = br.readLine();
				}
			} catch (IOException e) {
				try {
					br.close();
				} catch (IOException e1) {
					e1.printStackTrace();
				}
				throw new YunQueException("作业文件配置出错，详情见用户手册配置....", e);
			}
			jobMap = JSON.parseObject(jsonBuffer.toString().trim(),Map.class);
		} else if("YAML".equals(fileFormat)) {
			Yaml yaml = new Yaml();
			jobMap = yaml.load(br);
		} else {
			try {
				br.close();
			} catch (IOException e1) {
				e1.printStackTrace();
			}
			throw new YunQueException("运行模式参数 -fileFormat 取值 <JSON 或者 YAML>....");
		}
		logger.info("解析配置文件....");
		logger.info("加载Transformer插件....");
		List<TransformerExecution> transformerExecutionList = null;
		if (jobMap.get(ConfigConstant.TRANSFORMER) != null) {
			List<TransformerInfo> transformerInfos= JSONArray
					.parseArray(JSONArray.toJSONString(jobMap.get(ConfigConstant.TRANSFORMER)),TransformerInfo.class);
			transformerExecutionList = TransformerUtil.buildTransformerInfo(transformerInfos);
		}
		Map<String, String> readerConfig = jobMap.get(ConfigConstant.READER);
		Map<String, String> writerConfig = jobMap.get(ConfigConstant.WRITER);
		ConfigBean readerConfigBean = JSON.parseObject(JSON.toJSONString(readerConfig), ConfigBean.class);
		ConfigBean writerConfigBean = JSON.parseObject(JSON.toJSONString(writerConfig), ConfigBean.class);
		try {
			DBUtil.checkConfig(readerConfigBean);
		} catch (Exception e ){
			e.printStackTrace();
			logger.error(e.getMessage(), e);
			return  "readerConfig 配置错误 : "+ readerConfigBean;
		};

		try {
			DBUtil.checkConfig(writerConfigBean);
		} catch (Exception e ){
			e.printStackTrace();
			return "writerConfig 配置错误 : " + writerConfigBean;
		};

		String readerPlugin = readerConfig.get(ConfigConstant.READER_PLUGIN);
		String writerPlugin = writerConfig.get(ConfigConstant.WRITER_PLUGIN);



		logger.info("获取Reader和Writer....");
		Reader reader = Reader.getReaderPlugin(readerPlugin,readerConfigBean);
		Writer writer = Writer.getWriterPlugin(writerPlugin,writerConfigBean);


		logger.info("进行读写任务....");
		//通过new KafkaChannel 切换队列
		/*Map<String, String> kafkaConfig = jobMap.get(ConfigConstant.KAFKA);
		Channel channel = new KafkaChannel(kafkaConfig.get(ConfigConstant.HOST),kafkaConfig.get(ConfigConstant.TOPIC),kafkaConfig.get(ConfigConstant.CLIENTID),kafkaConfig.get(ConfigConstant.GROUPID));*/
		ChannelV2 channel=new DefaultChannelV2(transformerExecutionList);
		channel.channel(reader, writer, pos, count);

//		taskLogRecord.end();

		System.exit(ExitCode.CALLBACKEXIT.getExitCode());


		return "yunque 执行成功!";
	}
}
